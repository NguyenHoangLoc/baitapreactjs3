import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h3 className="card-text">${price}</h3>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.addToCart(this.props.data);
              }}
              className="btn btn-success mb-3 mx-2"
            >
              Add to Cart
            </button>
            <button
              onClick={() => {
                this.props.showDetails(this.props.data);
              }}
              className="btn btn-dark mb-3"
            >
              Show Details
            </button>
          </div>
        </div>
      </div>
    );
  }
}
