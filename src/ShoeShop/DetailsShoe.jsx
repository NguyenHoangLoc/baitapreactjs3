import React, { Component } from "react";

export default class DetailsShoe extends Component {
  render() {
    let { image, name, description, price, id } = this.props.details;
    return (
      <div className="row mt-5 alert-secondary p-5 text-left">
        <img src={image} alt="" className="col-3" />
        <div className="col-9 ">
          <p>ID: {id}</p>
          <h5>Name: {name}</h5>
          <p>Desc: {description}</p>
          <h6>Price: ${price}</h6>
        </div>
      </div>
    );
  }
}
