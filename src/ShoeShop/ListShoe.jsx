import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          addToCart={this.props.addToCart}
          showDetails={this.props.showDetails}
          data={item}
          key={item.id}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
