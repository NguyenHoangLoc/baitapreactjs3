import React, { Component } from "react";
export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img
              style={{
                width: "50px",
              }}
              src={item.image}
              alt=""
            />
          </td>
          <td>${item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.decreaseQuantity(item.id);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.increaseQuantity(item.id);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr className="bg-danger text-white">
            <td>ID</td>
            <td>NAME</td>
            <td>IMG</td>
            <td>PRICE</td>
            <td>QUANTITY</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
