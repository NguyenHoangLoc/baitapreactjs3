import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ItemShoe from "./ItemShoe";
import Cart from "./Cart";
import DetailsShoe from "./DetailsShoe";
import ListShoe from "./ListShoe";

export default class ExShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    details: dataShoe[0],
    cart: [],
  };

  showDetails = (shoe) => {
    this.setState({ details: shoe });
  };
  addToCart = (shoe) => {
    let newCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let cartItem = { ...shoe, soLuong: 1 };
      newCart.push(cartItem);
    } else {
      newCart[index].soLuong++;
    }

    this.setState({
      cart: newCart,
    });
  };
  increaseQuantity = (shoeId) => {
    let newCart = [...this.state.cart];
    let index = newCart.findIndex((item) => {
      return item.id === shoeId;
    });
    newCart[index].soLuong++;
    this.setState({
      cart: newCart,
    });
  };
  decreaseQuantity = (shoeId) => {
    let newCart = [...this.state.cart];
    let index = newCart.findIndex((item) => {
      return item.id === shoeId;
    });
    newCart[index].soLuong--;
    if (newCart[index].soLuong === 0) {
      newCart.splice(index, 1);
    }
    this.setState({
      cart: newCart,
    });
  };
  render() {
    // console.table(this.state.shoeArr);

    return (
      <div className="container">
        <Cart
          increaseQuantity={this.increaseQuantity}
          decreaseQuantity={this.decreaseQuantity}
          cart={this.state.cart}
        />
        <ListShoe
          addToCart={this.addToCart}
          shoeArr={this.state.shoeArr}
          renderListShoe={this.renderListShoe}
          showDetails={this.showDetails}
        />
        <DetailsShoe details={this.state.details} />
      </div>
    );
  }
}
